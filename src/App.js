import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import NavbarStore from './components/NavbarStore';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Paper from '@material-ui/core/Paper';
import SideBar from './components/SideBar';
import Drawer from '@material-ui/core/Drawer';

const useStyles = makeStyles(theme => ({
    grid: {
        marginTop: theme.spacing(0)
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: 'white',
        backgroundColor: theme.palette.secondary.light
    },
    list: {
        width: 250
    }
}));

function App() {
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const toggleDrawer = (side, open) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [side]: open });
    };

    const classes = useStyles();

    return (
        <React.Fragment>
            <CssBaseline />
            <NavbarStore toggleDrawer = {toggleDrawer}/>
            <Drawer open={state.left} onClose={toggleDrawer('left', false)}>
                <SideBar classes={classes} toggleDrawer={toggleDrawer} side = 'left'/>
            </Drawer>
                
            <Hidden>
                <Container maxWidth="lg">
                    <Grid className={classes.grid} container spacing={5} >
                        <Grid item xs={12} sm={6} md={4} lg={3}>
                            <Paper className={classes.paper}>1</Paper>
                        </Grid>
                        <Grid item xs={12} sm={6} md={4} lg={3}>
                            <Paper className={classes.paper}>2</Paper>
                        </Grid>
                        <Grid item xs={12} sm={6} md={4} lg={3}>
                            <Paper className={classes.paper}>3</Paper>
                        </Grid>
                        <Grid item xs={12} sm={6} md={4} lg={3}>
                            <Paper className={classes.paper}>4</Paper>
                        </Grid>
                    </Grid>
                </Container>
            </Hidden>
        </React.Fragment>
    );
}

export default App;
