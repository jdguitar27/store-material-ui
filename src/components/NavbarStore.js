import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import colors from '@material-ui/core/colors/purple';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: colors['500'],
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

class NavbarStore extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: useStyles(props)
    }
  }

  rendern() {return (
    <div className={this.classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={this.state.classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon onClick={this.props.toggleDrawer('left', true)} />
          </IconButton>
          <Typography variant="h6" className={this.state.classes.title}>
            News
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </div>
  )};
}

export default NavbarStore;
