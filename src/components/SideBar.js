import React from 'react';
import {
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	Divider
} from '@material-ui/core';
import MailIcon from '@material-ui/icons/Mail';
import InboxIcon from '@material-ui/icons/MoveToInbox';

class SideBar extends React.Component {
	render() {
		return (
			<div
				className = {this.props.classes.list}
				role = "presentation"
				onClick = {this.props.toggleDrawer(this.props.side, false)}
				onKeyDown = {this.props.toggleDrawer(this.props.side, false)}
			>
				<List>
					{['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
						<ListItem button key={text}>
							<ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
							<ListItemText primary={text} />
						</ListItem>
					))}
				</List>
				<Divider/>
				<List>
					{['All mail', 'Trash', 'Spam'].map((text, index) => (
					<ListItem button key={text}>
						<ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
						<ListItemText primary={text} />
					</ListItem>
					))}
				</List>
			</div>
		);
	}
}

export default SideBar;
